<?php get_header() ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<h1 style="color:<?= get_field('couleur')?> "><?php the_title() ?></h1>


<ul>
    <?php foreach(get_terms(['gamme']) as $term): ?>
        <li>
            <a href="<?= get_term_link($term) ?>">
                <?= $term->name?>
            </a>
        </li>
    <?php endforeach ?>
</ul>

<!--    --><?php //the_taxonomies();?>

<?php endwhile; endif ?>


<?php get_footer() ?>
