<?php get_header() ?>


<div class="container">

    <h1>Tous les articles</h1>


    <div class="row">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-md-6">
                <article>
                    <?php the_post_thumbnail('medium'); ?>

                    <h2><?php the_title() ?></h2>
                    <p><?php the_excerpt(); ?></p>
                    <small><?php the_author() ?></small>
                    <time datetime="<?php the_date() ?>"><?php the_time() ?></time>
                    <a href="<?php the_permalink(); ?>">coucou c'est une balise perso !!</a>
                </article>
            </div>
        <?php endwhile; endif ?>
    </ul>
</div>

<?php get_footer() ?>
