<?php get_header()?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article>
        <?php the_post_thumbnail('full');?>
        <h1><?php the_title() ?></h1>
        <p><?php the_content() ?></p>
        <small><?php the_author() ?></small>
        <time datetime="<?php the_date() ?>"><?php the_time() ?></time>
    </article>
<?php endwhile; endif ?>

<?php get_footer()?>
