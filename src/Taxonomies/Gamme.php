<?php

namespace MonTheme\Taxonomies;

use MonTheme\PostType\Product;

class Gamme
{
    public static function register()
    {
        add_action('init', [self::class, 'register_gamme' ]);
    }

    public static function register_gamme()
    {
        register_taxonomy('gamme', [Product::KEY], [
            'labels' => [
                'name' => 'Gamme',
                'singular_name' => 'Gamme',
                'add_new' => 'Ajouter une nouvelle gamme',
                'edit_item' => 'Modifier la gamme',
                'view_item' => 'Voir la gamme',
            ],
            'description' => 'Une gamme de produit',
            'public' => true,
            'hierarchical' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
        ]);
    }
}
