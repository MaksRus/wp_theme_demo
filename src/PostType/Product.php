<?php

namespace MonTheme\PostType;

class Product
{
    const KEY = 'product';

    public static function register()
    {
        add_action('init', [self::class, 'montheme_register_products']);
    }

    public static function montheme_register_products()
    {
        register_post_type(self::KEY, [
            'label' => 'Produits',
//        'labels' => [
//            'add_new' => 'Ajouter un nouveau produit',
//            'edit_item' => 'Modifier le produit',
//        ]
            'description' => 'Les produits en vente sur le site',
            'public' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-products',
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'comments',
            ],
            "taxonomies" => [],
            'has_archive' => true,
        ]);
    }

}
