<?php

namespace MonTheme;

class AssetsLoader
{
    public static function register()
    {
        add_action('wp_enqueue_scripts', [self::class, 'register_bootstrap']);
        add_action('wp_enqueue_scripts', [self::class, 'montheme_register_assets']);
    }

    public static function register_bootstrap()
    {
        wp_register_style(
            'bootstrap',
            get_template_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.css'
        );
        wp_register_script(
            'bootstrap',
            get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.js'
        );

        wp_enqueue_style('bootstrap');
        wp_enqueue_script('bootstrap');
    }

    public static function montheme_register_assets()
    {
        wp_register_style(
            'montheme',
            get_template_directory_uri() . '/assets/styles/css/main.css'
        );
        wp_register_script(
            'montheme',
            get_template_directory_uri() . '/assets/scripts/main.js',
        );

        wp_enqueue_style('montheme');
        wp_enqueue_script('montheme');
    }
}
