<?php
namespace MonTheme\Menus;

class Header
{
    const KEY = 'header';
    const DESCRIPTION = 'Menu du haut';

    public static function init()
    {
        add_action('after_setup_theme', [self::class, 'register']);
        add_filter('nav_menu_css_class', [self::class, 'li_css_classes']);
        add_filter('nav_menu_link_attributes', [self::class, 'change_menu_link_attr']);
    }

    public static function register()
    {
        register_nav_menu(self::KEY,  self::DESCRIPTION);
    }

    public static function li_css_classes($classes)
    {
        $classes[] = "nav-item";
        return $classes;
    }

    public static function change_menu_link_attr($atts)
    {
        $atts['class'] = "nav-link";
        return $atts;
    }
}
