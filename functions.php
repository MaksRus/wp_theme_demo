<?php
require 'vendor/autoload.php';

use MonTheme\PostType\Product;
use MonTheme\Taxonomies\Gamme;
use MonTheme\AssetsLoader;
use MonTheme\Menus\Header;

// Scripts
AssetsLoader::register();
// Types
Product::register();
// Taxonomies
Gamme::register();
// Menus
Header::init();


function montheme_supports()
{
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
}

function register_my_menu()
{
    register_nav_menu('header', "Menu du haut");
    register_nav_menu('footer', "Menu du bas");
}

function change_separator($sep)
{
    return '|';
}

function change_excerpt_length($length)
{
    return 20;
}

function change_the_title($title)
{
    return '"' . mb_strtoupper($title) . '"';
}


// Actions
add_action('after_setup_theme', 'montheme_supports');

// Menus


// Filters
add_filter("document_title_separator", "change_separator");
add_filter("excerpt_length", "change_excerpt_length");
add_filter("the_title", "change_the_title");
